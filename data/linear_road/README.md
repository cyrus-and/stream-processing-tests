# Dataset

Use [LinearGenerator][] tool:

```console
javac LinearGenerator/src/com/walmart/linearroad/generator/*
java -cp LinearGenerator/src/ com.walmart.linearroad.generator.LinearGen -o linear_road.txt -x 2 -m 1
```

[LinearGenerator]: https://github.com/walmartlabs/LinearGenerator
