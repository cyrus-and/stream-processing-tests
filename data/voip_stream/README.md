# Dataset

Use the bundled tool (the first parameter is the number of unique telephone numbers and the second is the number of calls to generate):

```console
java -cp java/target/StreamProcessingTests-0.0.0.jar xyz.cardaci.util.tools.voip_stream.DatasetGenerator 1000000 10000000 >voip_stream.txt
```
