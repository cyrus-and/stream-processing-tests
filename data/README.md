# Datasets and configurations

This directory contains small datasets just for the sake of running the applications out-of-the-box. Instructions about how to generate each dataset can be found in each directory.

The `dataset` JSON field should be an absolute path or a path relative to the current `data` directory.
