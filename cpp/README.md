# C++ applications

## Dependencies

```
apt install libssl-dev
```

## Build

```
make initialize
make [release] [WINDFLOW_ROOT=/path/to/WindFlow/ FASTFLOW_ROOT=/path/to/FastFlow/]
```

## Run

```
$ bin/windflow_<application> <configuration_file>
```

## Logging and debugging

When compiled using the `debug` profile, export the `LOG_LEVEL` environment variable to enable verbose logging.

```console
$ export LOG_LEVEL=
```
