#include "linear_road/average_speed.hpp"
#include "linear_road/count_vehicles.hpp"
#include "linear_road/dispatcher.hpp"
#include "linear_road/last_average_speed.hpp"
#include "linear_road/toll_notification_cv.hpp"
#include "linear_road/toll_notification_las.hpp"
#include "linear_road/toll_notification_pos.hpp"
#include "util/configuration.hpp"
#include "util/drain_sink.hpp"
#include "util/line_reader_source.hpp"
#include "util/metric_group.hpp"
#include "util/topology.hpp"
#include <windflow.hpp>

template <typename Source, typename Sink>
static void run_default_variant(wf::PipeGraph &graph, Source &source, Sink &sink, const util::Configuration &configuration, bool chaining)
{
    // build nodes

    auto dispatcher = util::setup(
        "dispatcher",
        configuration,
        wf::FlatMap_Builder(linear_road::Dispatcher())
    ).build();

    auto toll_notification_pos = util::setup(
        "toll_notification_pos",
        configuration,
        wf::FlatMap_Builder(linear_road::TollNotificationPos())
    ).build();

    auto average_speed = util::setup(
        "average_speed",
        configuration,
        wf::FlatMap_Builder(linear_road::AverageSpeed())
    ).build();

    auto last_average_speed = util::setup(
        "last_average_speed",
        configuration,
        wf::FlatMap_Builder(linear_road::LastAverageSpeed())
    ).build();

    auto toll_notification_las = util::setup(
        "toll_notification_las",
        configuration,
        wf::FlatMap_Builder(linear_road::TollNotificationLas())
    ).build();

    auto count_vehicles = util::setup(
        "count_vehicles",
        configuration,
        wf::FlatMap_Builder(linear_road::CountVehicles())
    ).build();

    auto toll_notification_cv = util::setup(
        "toll_notification_cv",
        configuration,
        wf::FlatMap_Builder(linear_road::TollNotificationCv())
    ).build();

    // build topology

    if (chaining) {
        wf::MultiPipe &dispatcher_pipe = graph.add_source(source);
        dispatcher_pipe.chain(dispatcher);

        dispatcher_pipe.split([] (const linear_road::Dispatcher::Tuple &tuple) {
            static const std::vector<size_t> broadcast = {0, 1, 2};
            return broadcast;
        }, 3);

        //

        wf::MultiPipe &position_pipe = dispatcher_pipe.select(0);
        position_pipe.chain(toll_notification_pos);

        wf::MultiPipe &speed_pipe = dispatcher_pipe.select(1);
        speed_pipe.chain(average_speed);
        speed_pipe.chain(last_average_speed);
        speed_pipe.chain(toll_notification_las);

        wf::MultiPipe &count_pipe = dispatcher_pipe.select(2);
        count_pipe.chain(count_vehicles);
        count_pipe.chain(toll_notification_cv);

        //

        wf::MultiPipe &sink_pipe = position_pipe.merge(speed_pipe, count_pipe);
        sink_pipe.chain_sink(sink);
    } else {
        wf::MultiPipe &dispatcher_pipe = graph.add_source(source);
        dispatcher_pipe.add(dispatcher);

        dispatcher_pipe.split([] (const linear_road::Dispatcher::Tuple &tuple) {
            static const std::vector<size_t> broadcast = {0, 1, 2};
            return broadcast;
        }, 3);

        //

        wf::MultiPipe &position_pipe = dispatcher_pipe.select(0);
        position_pipe.add(toll_notification_pos);

        wf::MultiPipe &speed_pipe = dispatcher_pipe.select(1);
        speed_pipe.add(average_speed);
        speed_pipe.add(last_average_speed);
        speed_pipe.add(toll_notification_las);

        wf::MultiPipe &count_pipe = dispatcher_pipe.select(2);
        count_pipe.add(count_vehicles);
        count_pipe.add(toll_notification_cv);

        //

        wf::MultiPipe &sink_pipe = position_pipe.merge(speed_pipe, count_pipe);
        sink_pipe.add_sink(sink);
    }

    // start!
    graph.run();
    util::metric_group.dump_all();
}

int main(int argc, char *argv[])
{
    auto configuration = util::Configuration::from_args(argc, argv);
    auto dataset_path = configuration.get_tree()["dataset"].GetString();
    auto run_time = configuration.get_tree()["run_time"].GetInt();
    auto source_delay = configuration.get_tree()["source_delay"].GetInt();
    auto sampling_rate = configuration.get_tree()["sampling_rate"].GetInt();
    auto chaining = configuration.get_tree()["chaining"].GetBool();

    // build source and sink nodes

    auto source = util::setup(
        "source",
        configuration,
        wf::Source_Builder(util::LineReaderSource(run_time, source_delay, dataset_path))
    ).build();

    auto sink = util::setup(
        "sink",
        configuration,
        wf::Sink_Builder(util::DrainSink<linear_road::NotificationTuple>(sampling_rate))
    ).build();

    // build topology

    wf::PipeGraph graph(argv[0]);
    run_default_variant(graph, source, sink, configuration, chaining);
}
