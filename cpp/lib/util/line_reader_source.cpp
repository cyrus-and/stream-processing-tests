#include "line_reader_source.hpp"
#include "log.hpp"
#include "metric_group.hpp"
#include <chrono>
#include <thread>

namespace util {

std::ostream &operator <<(std::ostream &os, const LineReaderSource::Tuple &tuple)
{
    return os << tuple.ts << ' ' << *tuple.line;
}

LineReaderSource::LineReaderSource(int run_time_sec, int delay, const std::string &path)
    : run_time_sec_(run_time_sec)
    , delay_(delay)
    , index_(0)
    , counter_(0)
{
    read_all(path);
    epoch_ = wf::current_time_nsecs();
}

bool LineReaderSource::operator ()(Tuple &tuple, wf::RuntimeContext &rc)
{
    // introduce artificial delay
    if (delay_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(delay_));
    }

    // fetch the next item
    if (index_ == 0 || index_ >= data_.size()) {
        index_ = rc.getReplicaIndex();
    }
    const auto &line = data_.at(index_);
    index_ += rc.getParallelism();
    ++counter_;

    // fill timestamp and value
    tuple.ts = wf::current_time_nsecs();
    tuple.user_time = 0;
    tuple.line = &line;

    DEBUG_LOG("source::tuple " << tuple);

    // set the epoch for the first time
    if (counter_ == 1) {
        epoch_ = tuple.ts;
    }

    if (tuple.ts - epoch_ < run_time_sec_ * 1e9) { // nanoseconds
        // call this function again
        return true;
    } else {
        DEBUG_LOG("source::finished");

        // this is the last emitted tuple so metrics are gathered
        auto rate = counter_ / ((tuple.ts - epoch_) / 1e9); // per second
        throughput_.add(rate);
        util::metric_group.add("throughput", throughput_);
        return false;
    }
}

void LineReaderSource::read_all(const std::string &path) {
    std::ifstream fs(path);
    std::string line;

    while (std::getline(fs, line)) {
        data_.push_back(line);
    }
}

}
