.PHONY: build-release build-debug initialize clean

build-release: initialize
	$(MAKE) -C cpp/ release
	$(MAKE) -C java/ build

build-debug: initialize
	$(MAKE) -C cpp/ debug
	$(MAKE) -C java/ build

initialize:
	$(MAKE) -C cpp/ initialize

clean:
	$(MAKE) -C cpp/ clean
	$(MAKE) -C java/ clean
