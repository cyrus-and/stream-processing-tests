#!/usr/bin/env python3

import json
import matplotlib.pyplot as plt
import os
import sys

def load_metrics(path):
    with open(path) as fs:
        return [metric for metric in json.load(fs) if metric]

def plot_metric(metric, index, total, y_label):
    x = [0, 5, 25, 50, 75, 95, 100]
    y = list(map(lambda x: metric[str(x)], x))
    plt.subplot(total, 1, index)
    plt.xlabel('Percentage (%)')
    plt.ylabel(y_label)
    plt.axhline(y=metric['mean'], color='r', linestyle=':')
    plt.plot(x, y, 'o-')

def plot_metrics(title, metrics):
    labels = {
        'latency': 'Latency (us)',
        'throughput': 'Throughput (tps)'
    }

    plt.suptitle(title)

    for index, metric in enumerate(metrics, start=1):
        label = labels[metric['name']]
        plot_metric(metric, index, len(metrics), label)

    plt.show()

def process_file(path):
    title = os.path.splitext(os.path.basename(path))[0]
    metrics = load_metrics(path)
    plot_metrics(title, metrics)

def main(args):
    if len(args) == 0:
        print('Usage: <result_file>...')
        sys.exit(1)

    for result_file in args:
        process_file(result_file)

if __name__ == '__main__':
    main(sys.argv[1:])
