#!/bin/bash

set -e

user="${1:?Specify a GitHub username}"
repo="${2:?Specify a GitHub repository}"
root="${3:?Specify a destination directory}"
hash="${4:?Specify commit SHA}"

mkdir -p "$root"
cd "$root"

if [[ -d "$repo" ]]; then
    echo "Directory '$repo' already exists, delete it manually if needed" 1>&2
    exit 1
fi

wget -q "https://github.com/$user/$repo/archive/$hash.zip"
unzip "$hash.zip"
mv "$repo-$hash" "$repo"
rm "$hash.zip"
