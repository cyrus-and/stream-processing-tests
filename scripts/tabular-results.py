#!/usr/bin/env python3

import json
import os
import sys

def load_result(path):
    with open(path) as fs:
        result = {}
        application, configuration, framework, _ = os.path.basename(path).split('#')
        result['application'] = application
        result['configuration'] = configuration
        result['framework'] = framework
        array = [metric for metric in json.load(fs) if metric]
        for metric in array:
            if 'name' in metric:
                result[metric['name']] = metric
        return result

def process_directory(path):
    results = []
    with os.scandir(path) as it:
        for entry in it:
            if entry.name.endswith('.json'):
                result = load_result(entry.path)
                result['group'] = os.path.basename(os.path.normpath(path))
                results.append(result)
    return results

def process_directories(paths):
    results = []
    for result_directory in paths:
        results.extend(process_directory(result_directory))
    return results

def build_table(results):
    table = []
    for result in results:
        row = [
            result['application'],
            result['configuration'],
            result['framework'],
            result['group'],
            result['latency']['total'],
            round(result['throughput']['mean'], 3),
            round(result['latency']['0'], 3),
            round(result['latency']['50'], 3),
            round(result['latency']['100'], 3),
        ]
        table.append(row)
    table.sort()
    return table

def print_table(table):
    print(' '.join([
        'application',
        'configuration',
        'framework',
        'group',
        'sink_tuples',
        'throughput_tps',
        'min_latency_us',
        'median_latency_us',
        'max_latency_us']))

    for row in table:
        print(' '.join(map(str, row)))

def main(args):
    if len(args) == 0:
        print('Usage: <result_directory>... [| column -t]')
        sys.exit(1)

    results = process_directories(args)
    table = build_table(results)
    print_table(table)

if __name__ == '__main__':
    main(sys.argv[1:])
