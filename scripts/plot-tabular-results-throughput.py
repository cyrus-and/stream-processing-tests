#!/usr/bin/env python3

from matplotlib import rcParams
import matplotlib.pyplot as plt
import pandas as pd
import sys

def plot(path):
    rcParams.update({'figure.autolayout': True})
    df = pd.read_csv(path, delim_whitespace=True)
    keys = df.application + ':' + df.configuration + ':' + df.framework
    labels = (df.application + ' ' +
              ' [' + df.framework + '] ' +
              df.configuration.str.replace('test-', '').str.replace('-', ' ') + ' ' +
              df.group.str.replace('results_', ''))
    values = df.throughput_tps / 1000
    indices = {}
    cmap = plt.get_cmap('Set2')
    colors = cmap([indices.setdefault(key, len(indices) % cmap.N) for key in keys])
    plt.bar(labels, values, color=colors)
    plt.xlim(-1, len(values))
    plt.xticks(rotation=90)
    plt.ylabel('Throughput (ktps)')
    plt.grid(axis='y')
    plt.show()

def main(args):
    if len(args) != 1:
        print('Usage: <tabular_results>')
        sys.exit(1)

    plot(args[0])

if __name__ == '__main__':
    main(sys.argv[1:])
