#!/bin/bash

run_application() {
    local framework="${1:?Specify a framework name}"
    local application="${2:?Specify an application name}"
    local configuration="${3:?Specify a configuration file}"
    local root="${4:-.}"
    local configuration_name="$(basename "$configuration" '.json')"
    local id="${application}#${configuration_name}#${framework}"

    # run the test
    echo -e "\n--- ${id} ---\n"
    rm -f metric_*.json
    case "$framework" in
        'windflow_'*)
            set -x
            local framework_exe="${framework%_*}"
            local allocator="${framework#*_}"
            case "$allocator" in
                'glibc')
                    unset LD_LIBRARY_PATH
                    unset LD_PRELOAD
                    ;;
                'hoard')
                    export LD_LIBRARY_PATH="${root}/cpp/external/Hoard/src"
                    export LD_PRELOAD='libhoard.so'
                    ;;
                'jemalloc')
                    export LD_LIBRARY_PATH="${root}/cpp/external/jemalloc/lib"
                    export LD_PRELOAD='libjemalloc.so'
                    ;;
                'tbb')
                    export LD_LIBRARY_PATH="$(echo "${root}/cpp/external/tbb/build/"linux_*_release)"
                    export LD_PRELOAD='libtbbmalloc_proxy.so.2'
                    ;;
                *)
                    echo "Unknown allocator $allocator" >&2
                    return
                    ;;
            esac
            set -x
            "${root}/cpp/bin/${framework_exe}_${application}" "${configuration}"
            set +x
            unset LD_LIBRARY_PATH
            unset LD_PRELOAD
            ;;
        'flink'|'storm')
            set -x
            java -cp "${root}/java/target/StreamProcessingTests-0.0.0.jar" "xyz.cardaci.$framework.$application.Main" "${configuration}"
            set +x
            ;;
        *)
            echo "Unknown framework name $framework" >&2
            return
            ;;
    esac

    # combine the results and clean up
    local timestamp="$(date +'%Y-%m-%d@%H:%M:%S')"
    local output_metric_file="results/${id}#${timestamp}.json"
    mkdir -p results/
    {
        echo '['
        cat "${configuration}"
        for metric_file in metric_*.json; do
            echo ','
            cat "${metric_file}"
        done
        echo ']'
    } >"${output_metric_file}"
    rm -f metric_*.json
    echo -e "\n--- done > ${output_metric_file} ---\n"
}

run() {
    local root="$(dirname "$0")/.."
    local frameworks="${1:-windflow_glibc windflow_hoard windflow_jemalloc windflow_tbb flink storm}"
    shift
    local configurations=${@:-linear_road/*.json\ voip_stream/*.json} # XXX update for new applications

    set -e

    # force manual cleanup (fails if existing)
    mkdir results/

    # locate the configuration files semi-automatically
    for configuration in $configurations; do
        local application="$(dirname "$configuration")"
        local configuration_name="$(basename "$configuration" .json)"
        local framework_match="${configuration_name%%%*}"
        for framework in $frameworks; do
            if [[ "$framework_match" =~ ("$framework"|^'all'$) ||
                  ("$framework_match" =~ (^|'-')'windflow'('-'|$) && "$framework" =~ ^'windflow_') ]]; then
                run_application "$framework" "$application" "$configuration" "$root"
            fi
        done
    done
}

run "$@"
