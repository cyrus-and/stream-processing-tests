#!/bin/bash

set -e

if [[ "$#" < 1 ]]; then
    echo 'Usage: <n> "<space-separated-frameworks>" <configurations>...' 1>&2
    exit 1
fi

n="$1"
shift

for i in $(seq "$n"); do
    dir_name="$(printf 'results_%03d' "$i")"
    if [[ -d "$dir_name" ]]; then
        echo "$dir_name existing" 1>&2
        exit 1
    fi

    "$(dirname "$0")/run.sh" "$@"
    mv results/ "$dir_name"
done
