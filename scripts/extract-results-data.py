#!/usr/bin/env python3

import json
import os
import re
import sys

def load_single_file(path):
    with open(path) as fs:
        result = {}
        application, configuration, framework, _ = os.path.basename(path).split('#')
        result['application'] = application
        result['configuration'] = configuration
        result['framework'] = framework
        array = [metric for metric in json.load(fs) if metric]
        for metric in array:
            if 'name' in metric:
                result[metric['name']] = metric
        return result

def filter_directories(directories, application_r, framework_r, configuration_r):
    results = {}
    for directory in directories:
        with os.scandir(directory) as it:
            for entry in it:
                if entry.name.endswith('.json'):
                    application, configuration, framework, _ = os.path.basename(entry.path).split('#')
                    if (re.search(application_r, application) and
                        re.search(framework_r, framework) and
                        re.search(configuration_r, configuration)):
                        key = (application, configuration, framework)
                        runs = results.setdefault(key, [])
                        runs.append(entry.path)
    return results

def load_data(results):
    for key, files in results.items():
        files[:] = map(load_single_file, files)

def pick_best_run(plot_type, results):
    assert plot_type in ('L', 'T', 'R')
    out = {}
    for key, data in results.items():
        if plot_type == 'L':
            # pick the lowest latency run
            best = min(data, key=lambda x: x['latency']['50'])
            out[key] = '\\boxplot{{{}}};'.format('}{'.join(
                map(str, [best['latency']['5'],
                          best['latency']['25'],
                          best['latency']['50'],
                          best['latency']['75'],
                          best['latency']['95']])))
        if plot_type == 'T':
            # pick the highest throughput run
            best = max(data, key=lambda x: x['throughput']['mean'])
            out[key] = best['throughput']['mean']
        if plot_type == 'R':
            # pick the lowest latency run
            best = min(data, key=lambda x: x['latency']['50'])
            user_latency = best['user_latency']['50']
            latency = best['latency']['50']
            runtime_latency = latency - user_latency
            out[key] = '{}'.format(runtime_latency / latency * 100)
    return out

def main(args):
    if len(args) < 5:
        print('Usage: L|T|R <application> <framework> <configuration> <result_directory>...')
        sys.exit(1)

    plot_type = args[0]
    application_r = args[1]
    framework_r = args[2]
    configuration_r = args[3]
    directories = args[4:]

    results = filter_directories(directories, application_r, framework_r, configuration_r)
    load_data(results)
    to_plot = pick_best_run(plot_type, results)
    for key, data in to_plot.items():
        print('#'.join(key), data)

if __name__ == '__main__':
    main(sys.argv[1:])
