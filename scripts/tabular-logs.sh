#!/bin/bash

awk '
BEGIN            { print("Op", "Ts", "Td") }
/service time/   { ts=$4 }
/departure time/ { td=$4 }
ENDFILE {
    sub(".*/", "", FILENAME)
    print(FILENAME, ts, td)
}' "$@" | column -t
