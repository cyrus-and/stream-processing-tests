# Stream processing tests

## Build

```
make
```

## Run

Move to the `data/` directory then run:

```
../scripts/run-repeated.sh <n>
```

Results will be in the `results_*/` folders.

## Attribution

This benchmark is based on the code provided in [storm-applications][1] and [briskstream][2], copyright is retained by the original authors,

[1]: https://github.com/mayconbordin/storm-applications
[2]: https://github.com/Xtra-Computing/briskstream
