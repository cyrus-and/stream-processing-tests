package xyz.cardaci.storm.voip_stream.reordering;

import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import xyz.cardaci.common.voip_stream.CallDetailRecord;
import xyz.cardaci.common.voip_stream.Constants;
import xyz.cardaci.common.voip_stream.ODTDBloomFilter;
import xyz.cardaci.common.voip_stream.ScorerMap;
import xyz.cardaci.util.Log;

import java.util.Map;

public class ENCR extends BaseRichBolt {
    private static final Logger LOG = Log.get(ENCR.class);

    private OutputCollector outputCollector;

    private ODTDBloomFilter filter;

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        LOG.info("prepare");

        // initialize
        this.outputCollector = outputCollector;

        int numElements = Constants.ENCR_NUM_ELEMENTS;
        int bucketsPerElement = Constants.ENCR_BUCKETS_PER_ELEMENT;
        int bucketsPerWord = Constants.ENCR_BUCKETS_PER_WORD;
        double beta = Constants.ENCR_BETA;
        filter = new ODTDBloomFilter(numElements, bucketsPerElement, beta, bucketsPerWord);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("source", "timestamp", "calling_number", "answer_timestamp", "rate", "cdr", "ecr", "user_latency"));
    }

    @Override
    public void execute(Tuple tuple) {
        LOG.debug("tuple in: {}", tuple);

        long start = System.nanoTime();
        long user_latency = (long) tuple.getValueByField("user_latency");

        long timestamp = (long) tuple.getValueByField("timestamp");
        CallDetailRecord cdr = (CallDetailRecord) tuple.getValueByField("cdr");

        boolean newCallee = tuple.getBooleanByField("new_callee");

        if (cdr.callEstablished) {
            String caller = tuple.getStringByField("calling_number");

            // forward the tuple fro ECR
            double ecr = (double) tuple.getValueByField("ecr");
            if (ecr >= 0) {
                Values out = new Values(ScorerMap.ECR, timestamp, caller, cdr.answerTimestamp, ecr, cdr, -1.0, user_latency + (System.nanoTime() - start));
                outputCollector.emit(out);
                LOG.debug("tuple out: {}", out);
            }

            if (newCallee) {
                filter.add(caller, 1, cdr.answerTimestamp);
                double rate = filter.estimateCount(caller, cdr.answerTimestamp);

                Values out = new Values(ScorerMap.ENCR, timestamp, caller, cdr.answerTimestamp, rate, cdr, -1.0, user_latency + (System.nanoTime() - start));
                outputCollector.emit(out);
                LOG.debug("tuple out: {}", out);
            }
        }

        outputCollector.ack(tuple);
    }
}
