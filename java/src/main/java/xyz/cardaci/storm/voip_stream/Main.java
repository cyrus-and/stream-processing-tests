package xyz.cardaci.storm.voip_stream;

import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.storm.DrainSink;
import xyz.cardaci.util.storm.LineReaderSpout;
import xyz.cardaci.util.storm.Topology;
import xyz.cardaci.util.storm.TopologyBuilderHints;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Configuration configuration = Configuration.fromArgs(args);
        String variant = configuration.getTree().get("variant").textValue();

        // run the variant
        switch (variant) {
            case "default":
                runDefaultVariant(configuration);
                break;
            case "reordering":
                runReorderingVariant(configuration);
                break;
            case "forwarding":
                runForwardingVariant(configuration);
                break;
            default:
                System.err.println("Unknown variant");
                System.exit(1);
        }
    }

    static void runDefaultVariant(Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();

        // prepare the topology
        TopologyBuilder topologyBuilder = new TopologyBuilderHints(configuration);
        topologyBuilder.setSpout("source", new LineReaderSpout(runTime, sourceDelay, datasetPath));

        topologyBuilder.setBolt("parser", new Parser())
                .shuffleGrouping("source");
        topologyBuilder.setBolt("dispatcher", new Dispatcher())
                .fieldsGrouping("parser", new Fields("calling_number", "called_number"));

        topologyBuilder.setBolt("ct24", new CT24())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("global_acd", new GlobalACD(), 1) // XXX fixed parallelism degree
                .globalGrouping("dispatcher");
        topologyBuilder.setBolt("ecr24", new ECR24())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("acd", new ACD())
                .fieldsGrouping("ct24", new Fields("calling_number"))
                .allGrouping("global_acd")
                .fieldsGrouping("ecr24", new Fields("calling_number"));

        topologyBuilder.setBolt("ecr", new ECR())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("encr", new ENCR())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("url", new URL())
                .fieldsGrouping("ecr", new Fields("calling_number"))
                .fieldsGrouping("encr", new Fields("calling_number"));

        topologyBuilder.setBolt("pre_rcr", new PreRCR())
                .shuffleGrouping("dispatcher");
        topologyBuilder.setBolt("rcr", new RCR())
                .fieldsGrouping("pre_rcr", new Fields("key"));
        topologyBuilder.setBolt("fofir", new FoFiR())
                .fieldsGrouping("rcr", new Fields("calling_number"))
                .fieldsGrouping("ecr", new Fields("calling_number"));

        topologyBuilder.setBolt("score", new Score())
                .fieldsGrouping("acd", new Fields("calling_number"))
                .fieldsGrouping("fofir", new Fields("calling_number"))
                .fieldsGrouping("url", new Fields("calling_number"));

        topologyBuilder.setBolt("sink", new DrainSink(samplingRate))
                .shuffleGrouping("score");

        // start!
        Topology.submit(topologyBuilder, configuration);
    }

    static void runReorderingVariant(Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();

        // prepare the topology
        TopologyBuilder topologyBuilder = new TopologyBuilderHints(configuration);
        topologyBuilder.setSpout("source", new LineReaderSpout(runTime, sourceDelay, datasetPath));

        topologyBuilder.setBolt("parser", new Parser())
                .shuffleGrouping("source");
        topologyBuilder.setBolt("dispatcher", new Dispatcher())
                .fieldsGrouping("parser", new Fields("calling_number", "called_number"));
        topologyBuilder.setBolt("ecr", new xyz.cardaci.storm.voip_stream.reordering.ECR())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));

        topologyBuilder.setBolt("ct24", new CT24())
                .fieldsGrouping("ecr", new Fields("calling_number"));
        topologyBuilder.setBolt("global_acd", new GlobalACD(), 1) // XXX fixed parallelism degree
                .globalGrouping("ecr");
        topologyBuilder.setBolt("ecr24", new ECR24())
                .fieldsGrouping("ecr", new Fields("calling_number"));
        topologyBuilder.setBolt("acd", new ACD())
                .fieldsGrouping("ct24", new Fields("calling_number"))
                .allGrouping("global_acd")
                .fieldsGrouping("ecr24", new Fields("calling_number"));

        topologyBuilder.setBolt("encr", new xyz.cardaci.storm.voip_stream.reordering.ENCR())
                .fieldsGrouping("ecr", new Fields("calling_number"));
        topologyBuilder.setBolt("url", new URL())
                .fieldsGrouping("encr", new Fields("calling_number"));

        topologyBuilder.setBolt("pre_rcr", new PreRCR())
                .shuffleGrouping("ecr");
        topologyBuilder.setBolt("rcr", new RCR())
                .fieldsGrouping("pre_rcr", new Fields("key"));
        topologyBuilder.setBolt("fofir", new xyz.cardaci.storm.voip_stream.reordering.FoFiR())
                .fieldsGrouping("rcr", new Fields("calling_number"));

        topologyBuilder.setBolt("score", new Score())
                .fieldsGrouping("acd", new Fields("calling_number"))
                .fieldsGrouping("fofir", new Fields("calling_number"))
                .fieldsGrouping("url", new Fields("calling_number"));

        topologyBuilder.setBolt("sink", new DrainSink(samplingRate))
                .shuffleGrouping("score");

        // start!
        Topology.submit(topologyBuilder, configuration);
    }

    static void runForwardingVariant(Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();

        // prepare the topology
        TopologyBuilder topologyBuilder = new TopologyBuilderHints(configuration);
        topologyBuilder.setSpout("source", new LineReaderSpout(runTime, sourceDelay, datasetPath));

        topologyBuilder.setBolt("parser", new Parser())
                .shuffleGrouping("source");
        topologyBuilder.setBolt("dispatcher", new Dispatcher())
                .fieldsGrouping("parser", new Fields("calling_number", "called_number"));

        topologyBuilder.setBolt("ct24", new CT24())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("global_acd", new GlobalACD(), 1) // XXX fixed parallelism degree
                .globalGrouping("dispatcher");
        topologyBuilder.setBolt("ecr24", new ECR24())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("acd", new ACD())
                .fieldsGrouping("ct24", new Fields("calling_number"))
                .allGrouping("global_acd")
                .fieldsGrouping("ecr24", new Fields("calling_number"));

        topologyBuilder.setBolt("ecr", new ECR())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("encr", new ENCR())
                .fieldsGrouping("dispatcher", new Fields("calling_number"));
        topologyBuilder.setBolt("url", new xyz.cardaci.storm.voip_stream.forwarding.URL())
                .fieldsGrouping("fofir", new Fields("calling_number"))
                .fieldsGrouping("encr", new Fields("calling_number"));

        topologyBuilder.setBolt("pre_rcr", new PreRCR())
                .shuffleGrouping("dispatcher");
        topologyBuilder.setBolt("rcr", new RCR())
                .fieldsGrouping("pre_rcr", new Fields("key"));
        topologyBuilder.setBolt("fofir", new xyz.cardaci.storm.voip_stream.forwarding.FoFiR())
                .fieldsGrouping("rcr", new Fields("calling_number"))
                .fieldsGrouping("ecr", new Fields("calling_number"));

        topologyBuilder.setBolt("score", new Score())
                .fieldsGrouping("acd", new Fields("calling_number"))
                .fieldsGrouping("url", new Fields("calling_number"));

        topologyBuilder.setBolt("sink", new DrainSink(samplingRate))
                .shuffleGrouping("score");

        // start!
        Topology.submit(topologyBuilder, configuration);
    }
}
