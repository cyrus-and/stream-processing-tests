package xyz.cardaci.storm.voip_stream;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import xyz.cardaci.common.voip_stream.CallDetailRecord;
import xyz.cardaci.common.voip_stream.Constants;
import xyz.cardaci.common.voip_stream.ODTDBloomFilter;
import xyz.cardaci.common.voip_stream.ScorerMap;
import xyz.cardaci.util.Log;

import java.util.Map;

// TODO basically a copy of ECR with different constants
class ECR24 extends BaseRichBolt {
    private static final Logger LOG = Log.get(ECR24.class);

    private OutputCollector outputCollector;

    private ODTDBloomFilter filter;

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        LOG.info("prepare");

        // initialize
        this.outputCollector = outputCollector;

        int numElements = Constants.ECR24_NUM_ELEMENTS;
        int bucketsPerElement = Constants.ECR24_BUCKETS_PER_ELEMENT;
        int bucketsPerWord = Constants.ECR24_BUCKETS_PER_WORD;
        double beta = Constants.ECR24_BETA;
        filter = new ODTDBloomFilter(numElements, bucketsPerElement, beta, bucketsPerWord);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("source", "timestamp", "calling_number", "answer_timestamp", "rate", "cdr", "user_latency"));
    }

    @Override
    public void execute(Tuple tuple) {
        LOG.debug("tuple in: {}", tuple);

        long start = System.nanoTime();
        long user_latency = (long) tuple.getValueByField("user_latency");

        long timestamp = (long) tuple.getValueByField("timestamp");
        CallDetailRecord cdr = (CallDetailRecord) tuple.getValueByField("cdr");

        if (cdr.callEstablished) {
            String caller = cdr.callingNumber;
            // add numbers to filters
            filter.add(caller, 1, cdr.answerTimestamp);
            double ecr = filter.estimateCount(caller, cdr.answerTimestamp);

            Values out = new Values(ScorerMap.ECR24, timestamp, caller, cdr.answerTimestamp, ecr, cdr, user_latency + (System.nanoTime() - start));
            outputCollector.emit(out);
            LOG.debug("tuple out: {}", out);
        }

        outputCollector.ack(tuple);
    }
}
