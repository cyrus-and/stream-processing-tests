package xyz.cardaci.storm.linear_road;

import org.apache.storm.topology.TopologyBuilder;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.storm.DrainSink;
import xyz.cardaci.util.storm.LineReaderSpout;
import xyz.cardaci.util.storm.Topology;
import xyz.cardaci.util.storm.TopologyBuilderHints;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Configuration configuration = Configuration.fromArgs(args);
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();

        // prepare the topology
        TopologyBuilder topologyBuilder = new TopologyBuilderHints(configuration);
        topologyBuilder.setSpout("source", new LineReaderSpout(runTime, sourceDelay, datasetPath));
        topologyBuilder.setBolt("dispatcher", new Dispatcher()).shuffleGrouping("source");

        topologyBuilder.setBolt("average_speed", new AverageSpeed()).shuffleGrouping("dispatcher");
        topologyBuilder.setBolt("last_average_speed", new LastAverageSpeed()).shuffleGrouping("average_speed");
        topologyBuilder.setBolt("toll_notification_las", new TollNotificationLas()).shuffleGrouping("last_average_speed");

        topologyBuilder.setBolt("count_vehicles", new CountVehicles()).shuffleGrouping("dispatcher");
        topologyBuilder.setBolt("toll_notification_cv", new TollNotificationCv()).shuffleGrouping("count_vehicles");

        topologyBuilder.setBolt("toll_notification_pos", new TollNotificationPos()).shuffleGrouping("dispatcher");

        topologyBuilder.setBolt("sink", new DrainSink(samplingRate))
                .shuffleGrouping("toll_notification_las")
                .shuffleGrouping("toll_notification_cv")
                .shuffleGrouping("toll_notification_pos");

        // start!
        Topology.submit(topologyBuilder, configuration);
    }
}
