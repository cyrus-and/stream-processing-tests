package xyz.cardaci.flink.linear_road;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import xyz.cardaci.common.linear_road.PositionReport;
import xyz.cardaci.util.Log;

public class Dispatcher extends RichFlatMapFunction<Tuple3<Long, String, Long>, Tuple3<Long, PositionReport, Long>> {
    private static final Logger LOG = Log.get(Dispatcher.class);

    @Override
    public void flatMap(Tuple3<Long, String, Long> tuple, Collector<Tuple3<Long, PositionReport, Long>> collector) throws Exception {

        long start = System.nanoTime();
        long user_latency = tuple.getField(tuple.getArity() - 1);

        // fetch values from the tuple
        long timestamp = tuple.f0;
        String line = tuple.f1;
        LOG.debug("Dispatcher: {}", line);

        // parse the string
        String[] token = line.split(",");
        short type = Short.parseShort(token[0]);
        if (type == 0) { // TODO constant
            // build and emit the position report
            PositionReport positionReport = new PositionReport();
            positionReport.type = type;
            positionReport.time = Integer.parseInt(token[1]);
            positionReport.vid = Integer.parseInt(token[2]);
            positionReport.speed = Integer.parseInt(token[3]);
            positionReport.xway = Integer.parseInt(token[4]);
            positionReport.lane = Short.parseShort(token[5]);
            positionReport.direction = Short.parseShort(token[6]);
            positionReport.segment = Short.parseShort(token[7]);
            positionReport.position = Integer.parseInt(token[8]);
            collector.collect(new Tuple3<>(timestamp, positionReport, user_latency + (System.nanoTime() - start)));
        }
    }
}
