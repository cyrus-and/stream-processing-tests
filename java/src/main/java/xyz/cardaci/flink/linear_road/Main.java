package xyz.cardaci.flink.linear_road;

import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import xyz.cardaci.common.linear_road.AvgVehicleSpeedTuple;
import xyz.cardaci.common.linear_road.CountTuple;
import xyz.cardaci.common.linear_road.PositionReport;
import xyz.cardaci.common.linear_road.TollNotification;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.flink.DrainSink;
import xyz.cardaci.util.flink.LineReaderSource;
import xyz.cardaci.util.flink.Topology;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Configuration configuration = Configuration.fromArgs(args);
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();
        boolean aggressiveChaining = configuration.getTree().get("aggressive_chaining").booleanValue();

        // initialize the environment
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();

        // flush as soon as possible in throttled mode (minimize the latency)
        if (configuration.getTree().get("source_delay").numberValue().longValue() != 0) {
            streamExecutionEnvironment.setBufferTimeout(0);
        }

        // prepare the topology
        SingleOutputStreamOperator<Tuple3<Long, PositionReport, Long>> dispatcher = streamExecutionEnvironment
                .addSource(new LineReaderSource(runTime, sourceDelay, datasetPath)).name("source")
                .setParallelism(Topology.getParallelismHint(configuration, "source"))
                .flatMap(new Dispatcher()).name("dispatcher")
                .setParallelism(Topology.getParallelismHint(configuration, "dispatcher"));

        SingleOutputStreamOperator<Tuple3<Long, AvgVehicleSpeedTuple, Long>> averageSpeed = dispatcher
                .flatMap(new AverageSpeed()).name("average_speed")
                .setParallelism(Topology.getParallelismHint(configuration, "average_speed"));

        if (!aggressiveChaining) {
            averageSpeed.startNewChain();
        }

        SingleOutputStreamOperator<Tuple3<Long, TollNotification, Long>> speedBranch = averageSpeed
                .flatMap(new LastAverageSpeed()).name("last_average_speed")
                .setParallelism(Topology.getParallelismHint(configuration, "last_average_speed"))
                .flatMap(new TollNotificationLas()).name("toll_notification_las")
                .setParallelism(Topology.getParallelismHint(configuration, "toll_notification_las"));

        ///

        SingleOutputStreamOperator<Tuple3<Long, CountTuple, Long>> countVehicles = dispatcher
                .flatMap(new CountVehicles()).name("count_vehicles")
                .setParallelism(Topology.getParallelismHint(configuration, "count_vehicles"));

        if (!aggressiveChaining) {
            countVehicles.startNewChain();
        }

        SingleOutputStreamOperator<Tuple3<Long, TollNotification, Long>> countBranch = countVehicles
                .flatMap(new TollNotificationCv()).name("toll_notification_cv")
                .setParallelism(Topology.getParallelismHint(configuration, "toll_notification_cv"));

        ///

        SingleOutputStreamOperator<Tuple3<Long, TollNotification, Long>> positionBranch = dispatcher
                .flatMap(new TollNotificationPos()).name("toll_notification_pos")
                .setParallelism(Topology.getParallelismHint(configuration, "toll_notification_pos"));

        if (!aggressiveChaining) {
            positionBranch.startNewChain();
        }

        ///

        positionBranch.union(speedBranch, countBranch)
                .addSink(new DrainSink<>(samplingRate)).name("sink")
                .setParallelism(Topology.getParallelismHint(configuration, "sink"));

        // start!
        Topology.submit(streamExecutionEnvironment, configuration);
    }
}
