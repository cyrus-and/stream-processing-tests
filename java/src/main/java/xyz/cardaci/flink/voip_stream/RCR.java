package xyz.cardaci.flink.voip_stream;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.api.java.tuple.Tuple9;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import xyz.cardaci.common.voip_stream.CallDetailRecord;
import xyz.cardaci.common.voip_stream.Constants;
import xyz.cardaci.common.voip_stream.ODTDBloomFilter;
import xyz.cardaci.common.voip_stream.ScorerMap;
import xyz.cardaci.util.Log;

public class RCR extends RichFlatMapFunction<
        Tuple9<String, Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>,
        Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>> {
    private static final Logger LOG = Log.get(RCR.class);

    private ODTDBloomFilter filter;

    @Override
    public void open(Configuration parameters) {
        LOG.info("open");

        int numElements = Constants.RCR_NUM_ELEMENTS;
        int bucketsPerElement = Constants.RCR_BUCKETS_PER_ELEMENT;
        int bucketsPerWord = Constants.RCR_BUCKETS_PER_WORD;
        double beta = Constants.RCR_BETA;
        filter = new ODTDBloomFilter(numElements, bucketsPerElement, beta, bucketsPerWord);
    }

    @Override
    public void flatMap(Tuple9<String, Long, String, String, Long, Boolean, CallDetailRecord, Double, Long> tuple, Collector<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>> collector) {
        LOG.debug("tuple in: {}", tuple);

        long start = System.nanoTime();
        long user_latency = tuple.getField(tuple.getArity() - 1);

        long timestamp = tuple.f1;
        CallDetailRecord cdr = tuple.f6;

        if (cdr.callEstablished) {
            String key = tuple.f0;

            // default stream
            if (key.equals(cdr.callingNumber)) {
                String callee = cdr.calledNumber;
                filter.add(callee, 1, cdr.answerTimestamp);
            }
            // backup stream
            else {
                String caller = cdr.callingNumber;
                double rcr = filter.estimateCount(caller, cdr.answerTimestamp);

                Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long> out = new Tuple8<>(ScorerMap.RCR, timestamp, caller, cdr.answerTimestamp, rcr, cdr, tuple.f7, user_latency + (System.nanoTime() - start));
                collector.collect(out);
                LOG.debug("tuple out: {}", out);
            }
        }
    }
}
