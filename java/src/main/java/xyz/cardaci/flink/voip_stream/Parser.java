package xyz.cardaci.flink.voip_stream;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.slf4j.Logger;
import xyz.cardaci.common.voip_stream.CallDetailRecord;
import xyz.cardaci.util.Log;

public class Parser extends RichMapFunction<
        Tuple3<Long, String, Long>,
        Tuple6<Long, String, String, Long, CallDetailRecord, Long>> {
    private static final Logger LOG = Log.get(Parser.class);

    @Override
    public Tuple6<Long, String, String, Long, CallDetailRecord, Long> map(Tuple3<Long, String, Long> tuple) {
        LOG.debug("tuple in: {}", tuple);

        long start = System.nanoTime();
        long user_latency = tuple.getField(tuple.getArity() - 1);

        // fetch values from the tuple
        long timestamp = tuple.f0;
        String line = tuple.f1;

        // parse the line
        CallDetailRecord cdr = new CallDetailRecord(line);

        Tuple6<Long, String, String, Long, CallDetailRecord, Long> out = new Tuple6<>(timestamp, cdr.callingNumber, cdr.calledNumber, cdr.answerTimestamp, cdr, user_latency + (System.nanoTime() - start));
        LOG.debug("tuple out: {}", out);
        return out;
    }
}
