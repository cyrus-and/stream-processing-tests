package xyz.cardaci.flink.voip_stream;

import org.apache.flink.api.java.tuple.*;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import xyz.cardaci.common.voip_stream.CallDetailRecord;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.flink.DrainSink;
import xyz.cardaci.util.flink.LineReaderSource;
import xyz.cardaci.util.flink.Topology;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Configuration configuration = Configuration.fromArgs(args);
        String variant = configuration.getTree().get("variant").textValue();

        // initialize the environment
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();

        // flush as soon as possible in throttled mode (minimize the latency)
        if (configuration.getTree().get("source_delay").numberValue().longValue() != 0) {
            streamExecutionEnvironment.setBufferTimeout(0);
        }

        // run the variant
        switch (variant) {
            case "default":
                runDefaultVariant(streamExecutionEnvironment, configuration);
                break;
            case "reordering":
                runReorderingVariant(streamExecutionEnvironment, configuration);
                break;
            case "forwarding":
                runForwardingVariant(streamExecutionEnvironment, configuration);
                break;
            default:
                System.err.println("Unknown variant");
                System.exit(1);
        }
    }

    private static void runDefaultVariant(StreamExecutionEnvironment streamExecutionEnvironment, Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();
        boolean aggressiveChaining = configuration.getTree().get("aggressive_chaining").booleanValue();

        // prepare the topology
        SingleOutputStreamOperator<Tuple3<Long, String, Long>> source = streamExecutionEnvironment
                .addSource(new LineReaderSource(runTime, sourceDelay, datasetPath)).name("source")
                .setParallelism(Topology.getParallelismHint(configuration, "source"));

        if (!aggressiveChaining) {
            source.startNewChain();
        }

        SingleOutputStreamOperator<Tuple8<Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> dispatcher = source
                .map(new Parser()).name("parser")
                .setParallelism(Topology.getParallelismHint(configuration, "parser"))
                .keyBy(1, 2) // both call numbers
                .map(new Dispatcher()).name("dispatcher")
                .setParallelism(Topology.getParallelismHint(configuration, "dispatcher"));

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ct24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new CT24()).name("ct24")
                .setParallelism(Topology.getParallelismHint(configuration, "ct24"))
                .keyBy(2); // calling number

        DataStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>> globalAcd = dispatcher
                .keyBy(1) // calling number
                .map(new GlobalACD()).name("global_acd")
                .setParallelism(1) // XXX fixed parallelism degree
                .broadcast();

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ecr24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ECR24()).name("ecr24")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr24"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> acd = ct24.union(globalAcd, ecr24)
                .flatMap(new ACD()).name("acd")
                .setParallelism(Topology.getParallelismHint(configuration, "acd"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple9<String, Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> preRcr = dispatcher
                .flatMap(new PreRCR()).name("pre_rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "pre_rcr"));

        if (!aggressiveChaining) {
            preRcr.disableChaining();
        }

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> rcr = preRcr
                .keyBy(0) // key field
                .flatMap(new RCR()).name("rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "rcr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> ecr = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ECR()).name("ecr")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> fofir = rcr.union(ecr)
                .flatMap(new FoFiR()).name("fofir")
                .setParallelism(Topology.getParallelismHint(configuration, "fofir"))
                .keyBy(2); // calling number

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> encr = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ENCR()).name("encr")
                .setParallelism(Topology.getParallelismHint(configuration, "encr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> url = ecr.union(encr)
                .flatMap(new URL()).name("url")
                .setParallelism(Topology.getParallelismHint(configuration, "url"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple6<Long, String, Long, Double, CallDetailRecord, Long>> score = acd.union(fofir, url)
                .map(new Score()).name("score")
                .setParallelism(Topology.getParallelismHint(configuration, "score"));

        score
                .addSink(new DrainSink<>(samplingRate)).name("sink")
                .setParallelism(Topology.getParallelismHint(configuration, "sink"));

        // start!
        Topology.submit(streamExecutionEnvironment, configuration);
    }

    private static void runReorderingVariant(StreamExecutionEnvironment streamExecutionEnvironment, Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();
        boolean aggressiveChaining = configuration.getTree().get("aggressive_chaining").booleanValue();

        // prepare the topology
        SingleOutputStreamOperator<Tuple3<Long, String, Long>> source = streamExecutionEnvironment
                .addSource(new LineReaderSource(runTime, sourceDelay, datasetPath)).name("source")
                .setParallelism(Topology.getParallelismHint(configuration, "source"));

        if (!aggressiveChaining) {
            source.startNewChain();
        }

        SingleOutputStreamOperator<Tuple8<Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> dispatcher = source
                .map(new Parser()).name("parser")
                .setParallelism(Topology.getParallelismHint(configuration, "parser"))
                .keyBy(1, 2) // both call numbers
                .map(new Dispatcher()).name("dispatcher")
                .setParallelism(Topology.getParallelismHint(configuration, "dispatcher"))
                .keyBy(1) // calling number
                .map(new xyz.cardaci.flink.voip_stream.reordering.ECR()).name("ecr")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr"));

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ct24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new CT24()).name("ct24")
                .setParallelism(Topology.getParallelismHint(configuration, "ct24"))
                .keyBy(2); // calling number

        DataStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>> globalAcd = dispatcher
                .keyBy(1) // calling number
                .map(new GlobalACD()).name("global_acd")
                .setParallelism(1) // XXX fixed parallelism degree
                .broadcast();

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ecr24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ECR24()).name("ecr24")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr24"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> acd = ct24.union(globalAcd, ecr24)
                .flatMap(new ACD()).name("acd")
                .setParallelism(Topology.getParallelismHint(configuration, "acd"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple9<String, Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> preRcr = dispatcher
                .flatMap(new PreRCR()).name("pre_rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "pre_rcr"));

        if (!aggressiveChaining) {
            preRcr.disableChaining();
        }

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> fofir = preRcr
                .keyBy(0) // key field
                .flatMap(new RCR()).name("rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "rcr"))
                .keyBy(2) // calling number
                .flatMap(new xyz.cardaci.flink.voip_stream.reordering.FoFiR()).name("fofir")
                .setParallelism(Topology.getParallelismHint(configuration, "fofir"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> url = dispatcher
                .keyBy(1) // calling number
                .flatMap(new xyz.cardaci.flink.voip_stream.reordering.ENCR()).name("encr")
                .setParallelism(Topology.getParallelismHint(configuration, "encr"))
                .keyBy(2) // calling number
                .flatMap(new URL()).name("url")
                .setParallelism(Topology.getParallelismHint(configuration, "url"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple6<Long, String, Long, Double, CallDetailRecord, Long>> score = acd.union(fofir, url)
                .map(new Score()).name("score")
                .setParallelism(Topology.getParallelismHint(configuration, "score"));

        score
                .addSink(new DrainSink<>(samplingRate)).name("sink")
                .setParallelism(Topology.getParallelismHint(configuration, "sink"));

        // start!
        Topology.submit(streamExecutionEnvironment, configuration);
    }

    private static void runForwardingVariant(StreamExecutionEnvironment streamExecutionEnvironment, Configuration configuration) {
        String datasetPath = configuration.getTree().get("dataset").textValue();
        long runTime = configuration.getTree().get("run_time").numberValue().longValue();
        long samplingRate = configuration.getTree().get("sampling_rate").numberValue().longValue();
        long sourceDelay = configuration.getTree().get("source_delay").numberValue().longValue();
        boolean aggressiveChaining = configuration.getTree().get("aggressive_chaining").booleanValue();

        // prepare the topology
        SingleOutputStreamOperator<Tuple3<Long, String, Long>> source = streamExecutionEnvironment
                .addSource(new LineReaderSource(runTime, sourceDelay, datasetPath)).name("source")
                .setParallelism(Topology.getParallelismHint(configuration, "source"));

        if (!aggressiveChaining) {
            source.startNewChain();
        }

        SingleOutputStreamOperator<Tuple8<Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> dispatcher = source
                .map(new Parser()).name("parser")
                .setParallelism(Topology.getParallelismHint(configuration, "parser"))
                .keyBy(1, 2) // both call numbers
                .map(new Dispatcher()).name("dispatcher")
                .setParallelism(Topology.getParallelismHint(configuration, "dispatcher"));

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ct24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new CT24()).name("ct24")
                .setParallelism(Topology.getParallelismHint(configuration, "ct24"))
                .keyBy(2); // calling number

        DataStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>> globalAcd = dispatcher
                .keyBy(1) // calling number
                .map(new GlobalACD()).name("global_acd")
                .setParallelism(1) // XXX fixed parallelism degree
                .broadcast();

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> ecr24 = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ECR24()).name("ecr24")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr24"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> acd = ct24.union(globalAcd, ecr24)
                .flatMap(new ACD()).name("acd")
                .setParallelism(Topology.getParallelismHint(configuration, "acd"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple9<String, Long, String, String, Long, Boolean, CallDetailRecord, Double, Long>> preRcr = dispatcher
                .flatMap(new PreRCR()).name("pre_rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "pre_rcr"));

        if (!aggressiveChaining) {
            preRcr.disableChaining();
        }

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> rcr = preRcr
                .keyBy(0) // key field
                .flatMap(new RCR()).name("rcr")
                .setParallelism(Topology.getParallelismHint(configuration, "rcr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> ecr = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ECR()).name("ecr")
                .setParallelism(Topology.getParallelismHint(configuration, "ecr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> fofir = rcr.union(ecr)
                .flatMap(new xyz.cardaci.flink.voip_stream.forwarding.FoFiR()).name("fofir")
                .setParallelism(Topology.getParallelismHint(configuration, "fofir"))
                .keyBy(2); // calling number

        KeyedStream<Tuple8<Integer, Long, String, Long, Double, CallDetailRecord, Double, Long>, Tuple> encr = dispatcher
                .keyBy(1) // calling number
                .flatMap(new ENCR()).name("encr")
                .setParallelism(Topology.getParallelismHint(configuration, "encr"))
                .keyBy(2); // calling number

        KeyedStream<Tuple7<Integer, Long, String, Long, Double, CallDetailRecord, Long>, Tuple> url = fofir.union(encr)
                .flatMap(new xyz.cardaci.flink.voip_stream.forwarding.URL()).name("url")
                .setParallelism(Topology.getParallelismHint(configuration, "url"))
                .keyBy(2); // calling number

        SingleOutputStreamOperator<Tuple6<Long, String, Long, Double, CallDetailRecord, Long>> score = acd.union(url)
                .map(new Score()).name("score")
                .setParallelism(Topology.getParallelismHint(configuration, "score"));

        score
                .addSink(new DrainSink<>(samplingRate)).name("sink")
                .setParallelism(Topology.getParallelismHint(configuration, "sink"));

        // start!
        Topology.submit(streamExecutionEnvironment, configuration);
    }
}
