package xyz.cardaci.common.linear_road;

public final class AvgVehicleSpeedTuple {
    public Integer vid;
    public Short minute;
    public Integer xway;
    public Short segment;
    public Short direction;
    public Integer avgSpeed;
}
