package xyz.cardaci.common.linear_road;

public class TollNotification {
    public Integer vid;
    public Integer speed;
    public Integer toll;
    public PositionReport pos;
}
