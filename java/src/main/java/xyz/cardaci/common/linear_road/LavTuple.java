package xyz.cardaci.common.linear_road;

public final class LavTuple {

    public Short minuteNumber;
    public Integer xway;
    public Short segment;
    public Short direction;
    public Integer lav;
}
