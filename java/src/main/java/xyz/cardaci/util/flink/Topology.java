package xyz.cardaci.util.flink;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;

public class Topology {
    private static final Logger LOG = Log.get(Topology.class);

    public static void submit(StreamExecutionEnvironment streamExecutionEnvironment, Configuration configuration) {
        try {
            // set up chaining
            boolean chaining = configuration.getTree().get("chaining").booleanValue();
            if (!chaining) {
                streamExecutionEnvironment.disableOperatorChaining();
            }

            // run the topology
            LOG.info("submitting topology");
            streamExecutionEnvironment.execute();

            // dump the metrics
            LOG.info("dumping metrics");
            MetricGroup.dumpAll();

            LOG.info("exiting");
        } catch (Exception e) {
            LOG.error(e.toString());
        }
    }

    public static int getParallelismHint(Configuration configuration, String name) {
        JsonNode jsonNode = configuration.getTree().get(name);
        int parallelismHint = jsonNode == null ? 1 : jsonNode.numberValue().intValue();
        LOG.info("NODE: {} ({})", name, parallelismHint);
        return parallelismHint;
    }
}
