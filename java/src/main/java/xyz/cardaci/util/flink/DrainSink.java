package xyz.cardaci.util.flink;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.slf4j.Logger;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;
import xyz.cardaci.util.Sampler;

public class DrainSink<T extends Tuple> extends RichSinkFunction<T> {
    private static final Logger LOG = Log.get(DrainSink.class);

    private final long samplingRate;

    private Sampler latency;
    private Sampler user_latency;

    public DrainSink(long samplingRate) {
        this.samplingRate = samplingRate;
    }

    @Override
    public void open(Configuration parameters) {
        LOG.info("open");
        latency = new Sampler(samplingRate);
        user_latency = new Sampler(samplingRate);
    }

    @Override
    public void invoke(Tuple tuple, Context context) {
        LOG.debug("tuple in: {}", tuple);

        assert tuple.getArity() > 2;

        long timestamp = tuple.getField(0);
        long user_latency_value = tuple.getField(tuple.getArity() - 1);
        long now = System.nanoTime();
        latency.add((now - timestamp) / 1e3, now); // microseconds
        user_latency.add(user_latency_value / 1e3, now); // microseconds
    }

    @Override
    public void close() {
        LOG.info("close");
        MetricGroup.add("latency", latency);
        MetricGroup.add("user_latency", user_latency);
    }
}
