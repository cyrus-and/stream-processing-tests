package xyz.cardaci.util.flink;

import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.slf4j.Logger;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;
import xyz.cardaci.util.Sampler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LineReaderSource extends RichParallelSourceFunction<Tuple3<Long, String, Long>> {
    private static final Logger LOG = Log.get(LineReaderSource.class);

    private String path;
    private long runTimeSec;
    private long delay;
    private List<String> data;

    private long counter;
    private Sampler throughput;

    public LineReaderSource(long runTimeSec, long delay, String path) {
        this.runTimeSec = runTimeSec;
        this.delay = delay;
        this.path = path;
    }

    @Override
    public void open(Configuration parameters) throws IOException {
        LOG.info("open");

        // initialize
        data = new ArrayList<>();
        counter = 0;
        throughput = new Sampler();

        // read the whole dataset
        readAll();
    }

    @Override
    public void run(SourceContext<Tuple3<Long, String, Long>> sourceContext) throws InterruptedException {
        long epoch = System.nanoTime();
        long timestamp = epoch;
        final int offset = getRuntimeContext().getIndexOfThisSubtask();
        final int stride = getRuntimeContext().getNumberOfParallelSubtasks();
        int index = 0;

        while (timestamp - epoch < runTimeSec * 1e9) {
            // introduce artificial delay
            if (delay != 0) {
                Thread.sleep(delay);
            }

            // fetch the next item
            if (index == 0 || index >= data.size()) {
                index = offset;
            }
            String line = data.get(index);

            // send the tuple
            timestamp = System.nanoTime();
            Tuple3<Long, String, Long> out = new Tuple3<>(timestamp, line, 0L);
            sourceContext.collect(out);
            LOG.debug("tuple out: {}", out);
            index += stride;
            counter++;
        }

        // dump metric
        double rate = counter / ((timestamp - epoch) / 1e9); // per second
        throughput.add(rate);
        MetricGroup.add("throughput", throughput);
    }

    @Override
    public void cancel() {
        LOG.info("cancel");
    }

    @Override
    public void close() {
        LOG.info("close");
    }

    private void readAll() throws IOException {
        // read the whole file line by line
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                data.add(line);
            }
        }
    }
}
