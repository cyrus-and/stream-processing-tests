package xyz.cardaci.util.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;
import xyz.cardaci.util.Sampler;

import java.util.Map;

public class DrainSink extends BaseRichBolt {
    private static final Logger LOG = Log.get(DrainSink.class);

    private final long samplingRate;

    private OutputCollector outputCollector;
    private Sampler latency;
    private Sampler user_latency;

    public DrainSink(long samplingRate) {
        this.samplingRate = samplingRate;
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        LOG.info("prepare");

        // initialize
        this.outputCollector = outputCollector;
        latency = new Sampler(samplingRate);
        user_latency = new Sampler(samplingRate);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    }

    @Override
    public void execute(Tuple tuple) {
        LOG.debug("tuple in: {}", tuple);

        long timestamp = (long) tuple.getValueByField("timestamp");
        long user_latency_value = (long) tuple.getValueByField("user_latency");
        long now = System.nanoTime();
        latency.add((now - timestamp) / 1e3, now); // microseconds
        user_latency.add(user_latency_value / 1e3, now); // microseconds

        outputCollector.ack(tuple);
    }

    @Override
    public void cleanup() {
        LOG.info("cleanup");

        MetricGroup.add("latency", latency);
        MetricGroup.add("user_latency", user_latency);
    }
}
