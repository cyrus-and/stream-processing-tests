package xyz.cardaci.util.storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import xyz.cardaci.util.Configuration;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;

public class Topology {
    private static final Logger LOG = Log.get(Topology.class);

    public static final String TOPOLOGY_NAME = "the_topology";

    private final static long POLLING_TIME_MS = 1000;
    private final static int BUFFER_SIZE = 32768; // XXX explicit default Storm value

    public static void submit(TopologyBuilder topologyBuilder, Configuration configuration) {
        // build the topology
        StormTopology topology = topologyBuilder.createTopology();

        // set the buffer size to avoid excessive buffering at the spout
        Config config = new Config();
        config.put(Config.TOPOLOGY_EXECUTOR_RECEIVE_BUFFER_SIZE, BUFFER_SIZE);

        // flush as soon as possible in throttled mode (minimize the latency)
        if (configuration.getTree().get("source_delay").numberValue().longValue() != 0) {
            config.put(Config.TOPOLOGY_PRODUCER_BATCH_SIZE, 1);
            config.put(Config.TOPOLOGY_TRANSFER_BATCH_SIZE, 1);
        }

        // submit it to storm
        try {
            LOG.info("submitting topology");

            // submit the topology to a local cluster
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology(TOPOLOGY_NAME, config, topology);

            // wait for termination
            LOG.info("waiting for topology termination");
            while (cluster.getNimbus().getClusterInfo().get_topologies_size() > 0) {
                Thread.sleep(POLLING_TIME_MS);
            }

            // kill cluster
            LOG.info("shutting down cluster");
            cluster.shutdown();

            // dump the metrics
            LOG.info("dumping metrics");
            MetricGroup.dumpAll();

            LOG.info("exiting");
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        // XXX force exit because the JVM may hang waiting for a dangling
        // reference...
        System.exit(0);
    }
}
