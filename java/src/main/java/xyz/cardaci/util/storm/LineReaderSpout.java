package xyz.cardaci.util.storm;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.thrift.TException;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import xyz.cardaci.util.Log;
import xyz.cardaci.util.MetricGroup;
import xyz.cardaci.util.Sampler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static xyz.cardaci.util.storm.Topology.TOPOLOGY_NAME;

public class LineReaderSpout extends BaseRichSpout {
    private static final Logger LOG = Log.get(LineReaderSpout.class);

    private SpoutOutputCollector spoutOutputCollector;

    private String path;
    private long runTimeSec;
    private long delay;
    private List<String> data;

    private boolean waitForShutdown;
    private int index;
    private int offset;
    private int stride;
    private long counter;
    private Sampler throughput;
    private long epoch, lastTupleTs;

    public LineReaderSpout(long runTimeSec, long delay, String path) {
        this.runTimeSec = runTimeSec;
        this.delay = delay;
        this.path = path;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("timestamp", "line", "user_latency"));
    }

    @Override
    public void open(Map<String, Object> map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        LOG.info("open");

        // initialize
        this.spoutOutputCollector = spoutOutputCollector;
        data = new ArrayList<>();
        offset = topologyContext.getThisTaskIndex();
        stride = topologyContext.getComponentTasks(topologyContext.getThisComponentId()).size();
        index = offset;
        counter = 0;
        throughput = new Sampler();

        // read the whole dataset
        try {
            readAll();
        } catch (IOException e) {
            spoutOutputCollector.reportError(e);
        }
    }

    @Override
    public void nextTuple() {
        // check termination
        if (waitForShutdown) {
            return;
        } else if (epoch > 0 && (System.nanoTime() - epoch) / 1e9 > runTimeSec) {
            try {
                LOG.info("killing topology");

                // initiate topology shutdown from inside
                NimbusClient nimbusClient = NimbusClient.getConfiguredClient(Utils.readStormConfig());
                nimbusClient.getClient().killTopology(TOPOLOGY_NAME);
                waitForShutdown = true;
                return;
            } catch (TException e) {
                spoutOutputCollector.reportError(e);
            }
        }

        // introduce artificial delay
        if (delay != 0) {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                spoutOutputCollector.reportError(e);
            }
        }

        // fetch the next item
        if (index >= data.size()) {
            index = offset;
        }
        String line = data.get(index);

        // send the tuple
        long timestamp = System.nanoTime();
        Values out = new Values(timestamp, line, 0L);
        spoutOutputCollector.emit(out);
        LOG.debug("tuple out: {}", out);
        lastTupleTs = timestamp;
        index += stride;
        counter++;

        // update the epoch the first time
        if (counter == 1) {
            epoch = System.nanoTime();
        }
    }

    @Override
    public void close() {
        LOG.info("close");

        double rate = counter / ((lastTupleTs - epoch) / 1e9); // per second
        throughput.add(rate);
        MetricGroup.add("throughput", throughput);
    }

    private void readAll() throws IOException {
        // read the whole file line by line
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                data.add(line);
            }
        }
    }
}
