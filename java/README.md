# Java applications

## Build

Build with `mvn package`, the artifact will be `target/StreamProcessingTests-0.0.0.jar` that depends on the `target/lib/` directory.

## Run

```
$ java -cp target/StreamProcessingTests-0.0.0.jar xyz.cardaci.storm.<application>.Main <configuration_file>
$ java -cp target/StreamProcessingTests-0.0.0.jar xyz.cardaci.flink.<application>.Main <configuration_file>
```

## Logging and debugging

By default the log level is set to `INFO` for this package and `ERROR` for everything else, this can be changed by the `LOG_LEVEL` and `ROOT_LOG_LEVEL` environment variables respectively, for example:

```console
$ export LOG_LEVEL=TRACE ROOT_LOG_LEVEL=WARN
```

## Avoid Java 9 "An illegal reflective access operation has occurred" warning

```console
$ export JAVA_TOOL_OPTIONS='--add-opens=java.base/java.nio=ALL-UNNAMED'
```
